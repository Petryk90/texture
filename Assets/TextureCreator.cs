﻿using UnityEngine;

public class TextureCreator : MonoBehaviour
{

    [Range(2, 512)]
    public int resolution = 256;

    private Texture2D texture;

    private MeshRenderer meshRenderer { get; set; }

    private void OnEnable()
    {
        if (texture == null)
        {
            meshRenderer = GetComponent<MeshRenderer>();
            texture = new Texture2D(resolution, resolution, TextureFormat.RGB24, true);
            texture.name = "Procedural Texture";
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.filterMode = FilterMode.Trilinear;
            texture.anisoLevel = 9;
            GetComponent<MeshRenderer>().material.mainTexture = texture;
        }
        FillTexture();
    }

    public void FillTexture()
    {
        //set up some test data. Make sure you actually have the datafile!
        string hgtPath = @"F:\C#\N48E024.hgt\N48E024.hgt";
        int chunksize = 601; //the width and height of the chunk we have read
        int gridsize = 1201;
        int posx = 0;
        int posy = 0;
        int[] output = srtmFile.ReadChunk(hgtPath, chunksize, gridsize, posx, posy);
        int maxHight = 9250; //maximum posible Mount(Everest) in the world - Read Sea     

        if (texture.width != resolution)
        {
            texture.Resize(chunksize, chunksize);
        }

        for (int y = 0; y < chunksize; y++)
        {
            for (int x = 0; x < chunksize; x++)
            {
                float stepSize = (maxHight - output[x + chunksize * y]) / 255/36.27f; //number for RGB color

                texture.SetPixel(x, y, new Color(stepSize, stepSize, stepSize)); //paint our texsture pixel by pixel
                
            }
        }

        texture.Apply();

        this.meshRenderer.material.mainTexture = texture;
    }
}