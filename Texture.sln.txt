﻿
Microsoft Visual Studio Solution File, Format Version 12.00
# Visual Studio 2015
Project("{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}") = "Texture.CSharp", "Texture.CSharp.csproj", "{EAA50611-2762-DB11-2217-CAB59DD62E6E}"
EndProject
Project("{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}") = "Texture.CSharp.Editor", "Texture.CSharp.Editor.csproj", "{057392F0-DBC1-3321-B98D-B3EE0162AB48}"
EndProject
Global
	GlobalSection(SolutionConfigurationPlatforms) = preSolution
		Debug|Any CPU = Debug|Any CPU
		Release|Any CPU = Release|Any CPU
	EndGlobalSection
	GlobalSection(ProjectConfigurationPlatforms) = postSolution
		{EAA50611-2762-DB11-2217-CAB59DD62E6E}.Debug|Any CPU.ActiveCfg = Debug|Any CPU
		{EAA50611-2762-DB11-2217-CAB59DD62E6E}.Debug|Any CPU.Build.0 = Debug|Any CPU
		{EAA50611-2762-DB11-2217-CAB59DD62E6E}.Release|Any CPU.ActiveCfg = Release|Any CPU
		{EAA50611-2762-DB11-2217-CAB59DD62E6E}.Release|Any CPU.Build.0 = Release|Any CPU
		{057392F0-DBC1-3321-B98D-B3EE0162AB48}.Debug|Any CPU.ActiveCfg = Debug|Any CPU
		{057392F0-DBC1-3321-B98D-B3EE0162AB48}.Debug|Any CPU.Build.0 = Debug|Any CPU
		{057392F0-DBC1-3321-B98D-B3EE0162AB48}.Release|Any CPU.ActiveCfg = Release|Any CPU
		{057392F0-DBC1-3321-B98D-B3EE0162AB48}.Release|Any CPU.Build.0 = Release|Any CPU
	EndGlobalSection
	GlobalSection(SolutionProperties) = preSolution
		HideSolutionNode = FALSE
	EndGlobalSection
EndGlobal
