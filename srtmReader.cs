﻿using System;
using System.IO;

namespace srtmReader
{
    public static class srtmFile
    {
        /// <summary>
        /// accesses a srtm files and reads a specific grid of data from it
        /// </summary>
        /// <param name="file">the path to the srtm file</param>
        /// <param name="chunksamples">the width and height of the chunk we want to read</param>
        /// <param name="gridsamples">the width and height of the total datagrid</param>
        /// <param name="posx">the x position of the chunk</param>
        /// <param name="posy">the y position of the chunk</param>
        /// <returns></returns>
        public static Int16[] ReadChunk(string file, int chunksamples, int gridsamples, int posx, int posy)
        {
            // setup the output to hold an array of integers
            Int16[] result = new Int16[(chunksamples * chunksamples)];
            using (FileStream fs = File.OpenRead(file))
            {
                //we're going to access one row of bytes at a time and will pick the int16 values from that
                byte[] rowBuffer = new byte[gridsamples * 2];
                //skip the rows we don't need
                fs.Seek((posy * gridsamples * 2), SeekOrigin.Begin);

                int theIndex = 0;
                for (int y = 0; y < chunksamples; y++)
                {
                    //read one row of data into the buffer
                    fs.Read(rowBuffer, 0, rowBuffer.Length);
                    //set up a loop to get the appropriate bytes from the row of data
                    for (int x = (posx * 2); x < (((posx + chunksamples) * 2)); x += 2, theIndex++)
                    {
                        //we're reading the data in reversed byte order.
                        //The data is big-endian, but we need to have little-endian.
                        result[theIndex] = (Int16)(rowBuffer[x] << 8 | rowBuffer[x + 1]);
                    }
                }
            }
            return result;
        }
    }
}